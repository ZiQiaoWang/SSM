package com.wzq.starter;

import java.util.List;

public abstract class TreeNode<T extends TreeNode>{

    public abstract Long getId();
    public abstract Long getParentId();
    public abstract List<T> getChildren();
    public abstract void setChildren(List<T> children);
}
