package com.wzq.starter;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import lombok.Data;

import java.util.Map;

@SuppressWarnings("ALL")
@Data
public class Response<T> {

    public static final Integer OK = 200;
    public static final Integer ERROR = 500;


    private Integer code;
    private String message;
    private String url;
    private T data;

    public static Response<String> success(){
        return success("success");
    }

    public static <T> Response<Map<String,T>> success(T data){
        return success("success!",data);
    }

    public static <T> Response<Map<String,T>> success(String message, T data){
        Response<Map<String,T>> info = new Response<>();
        info.setCode(OK);
        info.setMessage(message);
        Map<String,T> dataMap = Maps.newHashMap();
        dataMap.put("list",data);
        info.setData(dataMap);
        return info;
    }

    public static Response<String> success(String message){
        Response<String> info = new Response<>();
        info.setCode(OK);
        info.setMessage(message);
        return info;
    }

    public static <T> Response<T> failed() {
        return failed("操作失败！");
    }

    public static <T> Response<T> failed(String message){
        return failed(message,null);
    }

    public static <T> Response<T> failed(String message, T data){
        Response<T> info = new Response<T>();
        info.setCode(ERROR);
        info.setMessage(message);
        info.setData(data);
        return info;
    }

    public static Response<Map<String,Object>> page(PageInfo page){
        return page(page,OK,"success");
    }

    public static Response<Map<String,Object>> page(PageInfo page, Integer code, String message) {
        Response<Map<String,Object>> info = new Response<>();
        Map<String,Object> data = Maps.newHashMap();
        data.put("total",page.getTotal());
        data.put("list",page.getList());
        data.put("page",page.getPageNum());
        data.put("rows",page.getPageSize());
        info.setCode(code);
        info.setData(data);
        info.setMessage(message);
        return info;
    }
}