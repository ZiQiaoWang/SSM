package com.wzq.starter.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "url_authorities")
public class UrlAuthority {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    private String urlPattern;

    private String authorities;
}
