package com.wzq.starter.model;

import com.wzq.starter.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Menu extends TreeNode<Menu> {

    @Id
    private Long id;

    private Integer idx;

    private String css;

    private String href;

    private String name;

    private Long parentId;
    @Transient
    private List<Menu> children;
    @Transient
    private boolean active = false;
}