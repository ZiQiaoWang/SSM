package com.wzq.starter;

import com.github.pagehelper.PageInfo;

import java.util.List;

public class DataTablePage<T>{
    private Integer draw = 0;//请求计数器 这是为了防止跨站点脚本攻击（XSS）
    private long recordsTotal;//未经过过滤的数据记录数
    private long recordsFiltered;//过滤后的总的记录数
    private List<T> data;//要展示在表格中的数据，可以是一个对象数组，也可以是一个纯数组。区别在于，如果是纯数组的话，前端就不需要用columns绑定数据，会自动按照顺序去显示，每个数组中的数据就是一行；如果是对象数组，前端则需要使用columns绑定数据才能正常显示

    public DataTablePage(PageInfo<T> page) {
        super();
        this.setData(page.getList());
        this.setRecordsFiltered(page.getTotal());
        this.setRecordsTotal(page.getTotal());
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }


    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }
}