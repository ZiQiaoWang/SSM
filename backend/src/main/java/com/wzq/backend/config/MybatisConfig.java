package com.wzq.backend.config;

import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;

@Configuration
@MapperScan(basePackages = "com.wzq.backend.mapper")
public class MybatisConfig {
}
