package com.wzq.backend.system;

import com.wzq.starter.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Response defaultUrlErrorHandler(Exception e) {
        log.error(e.getMessage(),e);
        return Response.failed("系统开小差了,请稍后再试！",e.getCause());
    }

}

