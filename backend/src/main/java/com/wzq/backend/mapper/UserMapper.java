package com.wzq.backend.mapper;

import com.wzq.starter.model.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}
