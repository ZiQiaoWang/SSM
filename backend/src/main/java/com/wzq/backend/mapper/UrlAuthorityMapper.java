package com.wzq.backend.mapper;

import com.wzq.starter.model.UrlAuthority;
import tk.mybatis.mapper.common.Mapper;

public interface UrlAuthorityMapper extends Mapper<UrlAuthority> {
}
