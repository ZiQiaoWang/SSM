package com.wzq.backend.mapper;

import com.wzq.starter.model.Menu;
import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<Menu> {
}
