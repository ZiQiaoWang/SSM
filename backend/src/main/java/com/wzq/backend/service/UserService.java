package com.wzq.backend.service;


import com.wzq.backend.mapper.UserMapper;
import com.wzq.starter.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userDetailsService")
public class UserService extends JdbcUserDetailsManager {

    /**
     * 获取当前用户的方法
     * @return
     */
    public static User get(){
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public UserService(JdbcTemplate jdbcTemplate) {
        super.setJdbcTemplate(jdbcTemplate);
        this.setEnableGroups(true);
        this.setEnableAuthorities(false);
    }
    @Resource
    private UserMapper userMapper;

}
