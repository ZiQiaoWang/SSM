package com.wzq.backend.service;

import com.wzq.backend.mapper.UrlAuthorityMapper;
import com.wzq.starter.model.UrlAuthority;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SecurityService {

    @Resource
    private UrlAuthorityMapper urlAuthorityMapper;

    public List<UrlAuthority> loadAuthorities(){
        return urlAuthorityMapper.selectAll();
    }
}
