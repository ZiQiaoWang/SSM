import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import './plugins/element.js'
import '@/util/http.js'
import store from '@/util/store.js';
import 'font-awesome/css/font-awesome.min.css';

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
